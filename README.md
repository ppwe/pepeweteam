[![pipeline status](https://gitlab.com/ppwe/pepeweteam/badges/master/pipeline.svg)](https://gitlab.com/ppwe/pepeweteam/commits/master)

**Anggota Kelompok:**
Muhammad Farras Nur Aslam - 1806191433
Michael Eliazer - 1806191124
Shafira Ayu Maharani - 1806147174
Amilah Zahiroh - 1806185462

**Link Herokuapp:**
https://bookinc.herokuapp.com/

**About Book-Inc:**
	Book-Inc adalah website yang dibangun untuk kegiatan pinjam meminjam buku secara online. Pengguna Book-Inc dapat meminjam dan meminjamkan buku ke pengguna Book-Inc lainnya hanya melalui beberapa step. Untuk peminjaman buku, pengguna dapat log in atau sign up terlebih dahulu. Pengguna akan terhubung ke Homepage dan dapat menggunakan search bar untuk menemukan buku yang diinginkan. Pengguna kemudian akan di alihkan ke halaman deskripsi buku. Pada halaman ini, terdapat informasi mengenai buku tersebut dan juga tombol "Add". Jika pengguna menekan tombol Add, maka pengguna akan dialihkan ke halaman Form Peminjaman. Halaman form peminjaman akan secara otomatis menampilkan waktu pengembalian dan juga denda bila peminjam telat mengembalikan buku. 

**Features:**
- SearchBar di HomePage untuk mencari buku yang dapat dipinjam oleh user.
- Halaman Deskripsi menampilkan informasi buku dan tombol pinjam.
- Halaman Form Peminjaman
- Halaman History
