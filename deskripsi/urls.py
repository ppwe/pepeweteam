from django.urls import path
from . import views

app_name = 'deskripsi'

urlpatterns = [
    path('', views.deskripsi, name='deskripsi'),
    path('deskripsi2/', views.deskripsi2, name='deskripsi2'),
    path('deskripsi3/', views.deskripsi3, name='deskripsi3'),
    path('deskripsi4/', views.deskripsi4, name='deskripsi4')
    # dilanjutkan ...
]