from django import forms
from .models import deskripsi
from django.forms import ModelForm

class desk(forms.Form):
    penulis = forms.CharField(max_length=100)
    penerbit = forms.CharField(max_length=100)
    tahun_terbit = forms.CharField(max_length=100)
    pemilik = forms.CharField(max_length=100)
    genre = forms.CharField(max_length=100)
