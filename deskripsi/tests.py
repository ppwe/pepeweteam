from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import deskripsi, deskripsi2, deskripsi3, deskripsi4
from .forms import desk

class FormTest(TestCase):
	def test_url_exists(self):
		response = self.client.get('/deskripsi/')
		self.assertEqual(response.status_code, 200) 

	def test_uses_add_month_view(self):
		handler = resolve('/deskripsi/')
		self.assertEqual(handler.func, deskripsi)

	def test_uses_form_template(self):
		response = self.client.get('/deskripsi/')
		self.assertTemplateUsed(response, 'deskripsi.html')

	def test_ada_form(self):
		response = Client().get('/deskripsi/')
		content = response.content.decode('UTF8')
		self.assertIn('form', content)
		
	def test_penulisbuku(self):
		response = Client().get('/deskripsi/')
		content = response.content.decode('UTF8')
		self.assertIn('Penulis', content)

	def test_sinopsisbuku(self):
		response = Client().get('/deskripsi/')
		content = response.content.decode('UTF8')
		self.assertIn('Sinopsis', content)

	#def test_penerbitbuku(self):
	#    response = Client().get('/deskripsi/')
	# 	content = response.content.decode('UTF8')
	# 	self.assertIn('Penerbit', content)

	#def test_tahunterbit(self):
	# 	response = Client().get('/deskripsi/')
	# 	content = response.content.decode('UTF8')
	# 	self.assertIn('Tahun Terbit', content)

	def test_ada_button_Pinjam(self):
		response = Client().get('/deskripsi/')
		content = response.content.decode('UTF8')
		self.assertIn('Beli / Pinjam', content)
# Create your tests here.
