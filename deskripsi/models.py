from django.db import models

# Create your models here.
class deskripsi(models.Model):
    penulis = models.CharField(max_length=100, default = "")
    penerbit = models.CharField(max_length=100, default = "")
    tahun_terbit = models.CharField(max_length=100, default = "")
    pemilik = models.CharField(max_length=100, default = "")
    genre = models.CharField(max_length=100, default = "")
