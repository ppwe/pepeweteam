# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import add_saran,home
from .forms import FeedBackForm

class HomeTest(TestCase):
	def test_url_exists(self):
		response = self.client.get('')
		self.assertEqual(response.status_code, 200) 

	# def test_uses_add_saran_view(self):
	# 	handler = resolve('')
	# 	self.assertEqual(handler.func, Feea)

	def test_uses_home_template(self):
		response = self.client.get('')
		self.assertTemplateUsed(response, 'home.html')

	def test_ada_form(self):
		response = Client().get('')
		content = response.content.decode('UTF8')
		self.assertIn('form', content)
		# self.assertIn('<form', content)

	def test_subjudul_page_TERLARIS(self):
		response = Client().get('')
		content = response.content.decode('UTF8')
		self.assertIn('TERLARIS', content)

	def test_ada_subjudul_saran(self):
		response = Client().get('')
		content = response.content.decode('UTF8')   
		self.assertIn('Saran', content)

	def test_ada_button_Kirim(self):
		response = Client().get('')
		content = response.content.decode('UTF8')
		self.assertIn('Kirim', content)