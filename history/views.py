from django.shortcuts import render, redirect
from .forms import TanyaForm
from .models import Tanya
from form import models


# Create your views here.
def riwayat(request):
    list_riwayat = Tanya.objects.all().values()
    rr = models.PinjamBuku.objects.all().values()
    return render(request,"riwayat.html", {'Riwayat' : list_riwayat, 'out' : rr})

def add_riwayat(request):
    if request.method == 'POST' :
        form = TanyaForm(request.POST)
        if form.is_valid():
            tanya = form.data['pertanyaan']
            quest = Tanya(pertanyaan=tanya)
            quest.save()
    return redirect('/riwayat')