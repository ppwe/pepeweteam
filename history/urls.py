from django.urls import path
from . import views

app_name = 'history'

urlpatterns = [
    path('add_riwayat/', views.add_riwayat, name='add_riwayat'),
    path('', views.riwayat, name='riwayat'),
]