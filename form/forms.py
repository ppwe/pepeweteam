from django import forms
from form import models

class FormBuku(forms.Form):
	nama = forms.CharField(
		label = "Nama Peminjam",
		widget = forms.TextInput())
	tanggalPinjam = forms.DateField(
		label = "Tanggal Pinjam",
		input_formats = ['%d %m %Y'],
		widget = forms.DateInput())
