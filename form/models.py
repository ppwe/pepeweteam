from django.db import models

# Create your models here.
class PinjamBuku(models.Model):
	"""docstring for ClassName"""
	nama = models.CharField(max_length=50)
	tanggalPinjam = models.DateField()
	tanggalKembali = models.DateField()