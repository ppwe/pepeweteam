from django.urls import path,include
from . import views

urlpatterns = [
	path('', views.formPinjam, name='pinjam'),
	path('riwayat', views.riwayat, name='riwayat')
]
