from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import add_month, formPinjam
from .forms import FormBuku

class FormTest(TestCase):
	def test_url_exists(self):
		response = self.client.get('/pinjam/')
		self.assertEqual(response.status_code, 200)

	def test_uses_add_month_view(self):
		handler = resolve('/pinjam/')
		self.assertEqual(handler.func, formPinjam)

	def test_uses_form_template(self):
		response = self.client.get('/pinjam/')
		self.assertTemplateUsed(response, 'form.html')

	def test_ada_form(self):
		response = Client().get('/pinjam/')
		content = response.content.decode('UTF8')
		self.assertIn('form', content)
		self.assertIn('<form', content)

	def test_judul_page_FORM_PEMINJAMAN(self):
		response = Client().get('/pinjam/')
		content = response.content.decode('UTF8')
		self.assertIn('FORM', content)
		self.assertIn('PEMINJAMAN', content)

	def test_ada_isian_Nama_Buku(self):
		response = Client().get('/pinjam/')
		content = response.content.decode('UTF8')
		self.assertIn('Nama Peminjam', content)

	def test_ada_button_Pinjam(self):
		response = Client().get('/pinjam/')
		content = response.content.decode('UTF8')
		self.assertIn('Pinjam', content)
		


	




