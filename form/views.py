from django.shortcuts import render, redirect
from form import models, forms
import datetime
import calendar

def add_month(sourcedate):
	month = sourcedate.month
	year = sourcedate.year + month // 12
	month = month % 12 + 1
	day = min(sourcedate.day, calendar.monthrange(year,month)[1])
	return datetime.date(year, month, day)

def riwayat(request):
    list_riwayat = models.PinjamBuku.objects.all().values()
    return render(request,"riwayat.html", {'riwayat' : list_riwayat})

# Create your views here.
def formPinjam(request):
	if request.method == "POST":
		form = forms.FormBuku(request.POST)
		tglform = form.data['tanggalPinjam'].split()
		tanggal = tglform[2] + "-" + tglform[1] + "-" + tglform[0]
		if form.is_valid():
			baru = models.PinjamBuku(
				nama = form.data['nama'],
				tanggalPinjam = tanggal,
				tanggalKembali = add_month(datetime.date(int(tglform[2]), int(tglform[1]), int(tglform[0])))
				)
			baru.save()
			return redirect('/riwayat/')
	else:
		form = forms.FormBuku()

	content = {'formkosong' : form, 'isi' : models.PinjamBuku.objects.all().values()}
	return render(request, 'form.html', content)